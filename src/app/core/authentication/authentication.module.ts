import { NgModule } from '@angular/core';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { SignInComponent } from './containers/sign-in/sign-in.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [SignInComponent],
  imports: [
    AuthenticationRoutingModule,
    SharedModule
  ]
})
export class AuthenticationModule { }
