import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  signIn: FormGroup;
  constructor(private fb: FormBuilder) {
    this.signIn = fb.group({
      email: ['', Validators.required, Validators.email],
      password: ['', [ Validators.required]]
    });
   }

  ngOnInit(): void {
  }

}
