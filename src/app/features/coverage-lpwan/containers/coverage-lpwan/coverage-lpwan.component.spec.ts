import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoverageLpwanComponent } from './coverage-lpwan.component';

describe('CoverageLpwanComponent', () => {
  let component: CoverageLpwanComponent;
  let fixture: ComponentFixture<CoverageLpwanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoverageLpwanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverageLpwanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
