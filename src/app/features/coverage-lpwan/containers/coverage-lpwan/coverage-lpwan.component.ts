import { Component, OnInit } from '@angular/core';
import {GetdataService} from '../../services/getdata.service';

@Component({
  selector: 'app-coverage-lpwan',
  templateUrl: './coverage-lpwan.component.html',
  styleUrls: ['./coverage-lpwan.component.scss']
})
export class CoverageLpwanComponent implements OnInit {

  public data: any;

  public places: any;
  public coverage: any;


  public chartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(105, 0, 132, .2)',
      borderColor: 'rgba(200, 99, 132, .7)',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(0, 137, 132, .2)',
      borderColor: 'rgba(0, 10, 130, .7)',
      borderWidth: 2,
    }
  ];
  public chartOptions: any = {
    responsive: true
  };

  constructor(
    private getDataService: GetdataService
  ) { }

  ngOnInit(): void {
    this.getDataService.getLora().subscribe( data => {
      this.data = data;
      this.getDataService.getCoverage().subscribe( data2 => {
        this.coverage = data2;
        this.transformData();
        }
      );
    });
  }
  transformData() {
    const newArr3 = this.data.data.map( data => {
      return data.idp;
    });
    this.places = [...new Set(newArr3)];
    console.log(this.coverage)
  }


}
