import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CoverageLpwanComponent} from './containers/coverage-lpwan/coverage-lpwan.component';


const routes: Routes = [
  {
    path: '',
    component: CoverageLpwanComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoverageLpwanRoutingModule { }
