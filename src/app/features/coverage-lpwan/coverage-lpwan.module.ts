import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoverageLpwanRoutingModule } from './coverage-lpwan-routing.module';
import { CoverageLpwanComponent } from './containers/coverage-lpwan/coverage-lpwan.component';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [CoverageLpwanComponent],
  imports: [
    CommonModule,
    CoverageLpwanRoutingModule,
    SharedModule
  ]
})
export class CoverageLpwanModule { }
