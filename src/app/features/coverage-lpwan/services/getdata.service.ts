import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GetdataService {

  private backendUrl: string = environment.backendUrl;

  constructor(
    private httpClient: HttpClient
  ) { }
  getLora() {
    return this.httpClient.get(`${this.backendUrl}complora`);
  }
  getCoverage() {
    return this.httpClient.get(`${this.backendUrl}cobertura`);
  }
}
