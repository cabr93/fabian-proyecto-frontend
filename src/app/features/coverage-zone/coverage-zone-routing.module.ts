import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CoverageZoneComponent} from './containers/coverage-zone/coverage-zone.component';


const routes: Routes = [
  {
    path: '',
    component: CoverageZoneComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoverageZoneRoutingModule { }
