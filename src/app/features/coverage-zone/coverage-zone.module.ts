import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoverageZoneRoutingModule } from './coverage-zone-routing.module';
import { CoverageZoneComponent } from './containers/coverage-zone/coverage-zone.component';


@NgModule({
  declarations: [CoverageZoneComponent],
  imports: [
    CommonModule,
    CoverageZoneRoutingModule
  ]
})
export class CoverageZoneModule { }
