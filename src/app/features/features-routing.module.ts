import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FeaturesComponent} from './features.component';


const routes: Routes = [
  {
    path: '',
    component: FeaturesComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'battery',
        loadChildren: () => import('./battery/battery.module').then(m => m.BatteryModule)
      },
      {
        path: 'costs',
        loadChildren: () => import('./costs/costs.module').then(m => m.CostsModule)
      },
      {
        path: 'coverage-lpwan',
        loadChildren: () => import('./coverage-lpwan/coverage-lpwan.module').then(m => m.CoverageLpwanModule)
      },
      {
        path: 'coverage-zone',
        loadChildren: () => import('./coverage-zone/coverage-zone.module').then(m => m.CoverageZoneModule)
      },
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'implementation',
        loadChildren: () => import('./implementation/implementation.module').then(m => m.ImplementationModule)
      },
      {
        path: 'per-lpwan',
        loadChildren: () => import('./per-lpwan/per-lpwan.module').then(m => m.PerLpwanModule)
      },
      {
        path: 'per-zone',
        loadChildren: () => import('./per-zone/per-zone.module').then(m => m.PerZoneModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeaturesRoutingModule { }
