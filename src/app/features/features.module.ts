import { NgModule } from '@angular/core';

import { FeaturesRoutingModule } from './features-routing.module';
import { FeaturesComponent } from './features.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [FeaturesComponent],
  imports: [
    FeaturesRoutingModule,
    SharedModule
  ]
})
export class FeaturesModule { }
