import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ImplementationComponent} from './containers/implementation/implementation.component';


const routes: Routes = [
  {
    path: '',
    component: ImplementationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImplementationRoutingModule { }
