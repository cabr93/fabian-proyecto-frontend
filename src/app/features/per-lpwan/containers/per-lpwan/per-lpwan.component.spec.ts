import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerLpwanComponent } from './per-lpwan.component';

describe('PerLpwanComponent', () => {
  let component: PerLpwanComponent;
  let fixture: ComponentFixture<PerLpwanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerLpwanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerLpwanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
