import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PerLpwanComponent} from './containers/per-lpwan/per-lpwan.component';


const routes: Routes = [
  {
    path: '',
    component: PerLpwanComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerLpwanRoutingModule { }
