import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerLpwanRoutingModule } from './per-lpwan-routing.module';
import { PerLpwanComponent } from './containers/per-lpwan/per-lpwan.component';


@NgModule({
  declarations: [PerLpwanComponent],
  imports: [
    CommonModule,
    PerLpwanRoutingModule
  ]
})
export class PerLpwanModule { }
