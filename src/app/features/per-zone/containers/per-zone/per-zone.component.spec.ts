import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerZoneComponent } from './per-zone.component';

describe('PerZoneComponent', () => {
  let component: PerZoneComponent;
  let fixture: ComponentFixture<PerZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
