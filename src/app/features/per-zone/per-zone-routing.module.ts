import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PerZoneComponent} from './containers/per-zone/per-zone.component';


const routes: Routes = [
  {
    path: '',
    component: PerZoneComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerZoneRoutingModule { }
