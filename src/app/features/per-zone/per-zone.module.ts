import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerZoneRoutingModule } from './per-zone-routing.module';
import { PerZoneComponent } from './containers/per-zone/per-zone.component';


@NgModule({
  declarations: [PerZoneComponent],
  imports: [
    CommonModule,
    PerZoneRoutingModule
  ]
})
export class PerZoneModule { }
