import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'idData'
})
export class IdDataPipe implements PipeTransform {

  transform(value: any): unknown {
    return value.map( data => data.id);
  }

}
