import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'idp'
})
export class IdpPipe implements PipeTransform {

  transform(value: Array<any>, place: string): unknown {
    return value.filter( data => data.idp === place);
  }

}
