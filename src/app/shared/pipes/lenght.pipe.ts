import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lenght'
})
export class LenghtPipe implements PipeTransform {

  transform(value: any): unknown {
    return value.length;
  }

}
