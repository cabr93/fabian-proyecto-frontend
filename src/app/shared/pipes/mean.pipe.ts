import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mean'
})
export class MeanPipe implements PipeTransform {

  transform(value: any): unknown {
    console.log(value)
    let total = 0;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < value.length; i++) {
      total += Number(value[i].rssi);
    }
    console.log(total)
    return total / value.length;
  }

}
