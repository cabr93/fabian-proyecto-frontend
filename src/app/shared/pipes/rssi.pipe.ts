import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rssi'
})
export class RssiPipe implements PipeTransform {

  transform(value: any): unknown {
    return value.map( data => data.rssi);
  }

}
