import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { ReactiveFormsModule } from '@angular/forms';
import { IdpPipe } from './pipes/idp.pipe';
import { RssiPipe } from './pipes/rssi.pipe';
import { IdDataPipe } from './pipes/id-data.pipe';
import { MeanPipe } from './pipes/mean.pipe';
import { LenghtPipe } from './pipes/lenght.pipe';

const modules = [
  CommonModule,
  MDBBootstrapModulesPro,
  ReactiveFormsModule
]

@NgModule({
  declarations: [IdpPipe, RssiPipe, IdDataPipe, MeanPipe, LenghtPipe],
  imports: [
    ...modules
  ],
  exports: [
    ...modules,
    IdpPipe,
    RssiPipe,
    IdDataPipe,
    MeanPipe,
    LenghtPipe
  ]
})
export class SharedModule { }
